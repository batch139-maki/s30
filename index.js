const express = require('express');
const app = express();
const PORT = 4000;
const mongoose = require('mongoose');

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect('mongodb+srv://machiavelli:*secret*@batch139.73pcg.mongodb.net/User?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Successfully connected to database`));

// Schema
const userSchema = new mongoose.Schema({
    username: String,
    password: String
});

// Model
const userCreate = mongoose.model("User", userSchema);


app.get("/signup", (req, res) => res.send(`Welcome, pls sign up`));

app.post("/signup", (req, res) => {
    userCreate.findOne({username: req.body.username}, (err, result) => 
    {
        
        if(result != null && result.username == req.body.username) 
        {return res.send(`User already exist!`);} 
        else {let newUser = new userCreate(   
                {
                    username: req.body.username,
                    password: req.body.password
                })
            console.log(newUser)

            newUser.save((err, savedTask) => {
                if(err) {
                    return console.error(err)
                } else {
                    return res.send(`User ${req.body.username} succesfully register!`)
                }
            });
                }   

        });
    }
);


app.listen(PORT, () => console.log(`Server running at port ${PORT}`));
